echo
echo "Installing Java Development tools"
brew install jenv
#brew cask install java #changed to avoid Java 9 as default
brew tap caskroom/versions
brew cask install java8
#brew cask install intellij-idea --force # guard against pre-installed intellij
brew install maven
brew install gradle
brew install springboot

#source ${MY_DIR}/download-pivotal-ide-prefs.sh
#pushd ~/workspace/pivotal_ide_prefs/cli
#./bin/ide_prefs install --ide=intellij
#popd
